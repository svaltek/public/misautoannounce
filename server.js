/* eslint-disable no-console */
const misrcon = require('node-misrcon');
const rconconfig = require('./config');
const axios = require('axios');
const dodebug = false;
// Create ApiObject
// eslint-disable-next-line max-len
const server = new misrcon.NodeMisrcon({ ip: `${rconconfig.theIP}`, port: `${rconconfig.thePORT}`, password: `${rconconfig.thePASSWORD}` });
const commandResponse = server.send(`sv_say ${rconconfig.theMESSAGE}`);
if(dodebug) { console.log('Sending RCON Command...'); }
commandResponse
.then(response => {
	if(dodebug) {
		console.log(response);
	}
})
.catch(err => {
	console.error(err);
});
if(dodebug) { console.log('Posting to Discord...'); }
axios.post(`${rconconfig.webhookURL}`, {
	username: `${rconconfig.webhookUSER}`, content: `${rconconfig.theMESSAGE}`
})
.then(res => {
	if(dodebug) {
		console.log(`statusCode: ${res.statusCode}`);
		console.log(res);
	}
})
.catch(error => {
	console.error(error);
});
