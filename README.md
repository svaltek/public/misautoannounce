MisAutoAnnounce
---------------
installation:
---
- clone the repo
- change to the cloned directory
- run `npm install`

Usage
---
- copy config.js.example to config.js and set correct values
- run `npm start` or `node server.js` from root directory